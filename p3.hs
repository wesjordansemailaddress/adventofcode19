import System.IO  
import Control.Monad
import Data.Maybe

type Point = (Int, Int)
type LineSegment = (Point, Point)

within ::  Int -> (Int, Int) -> Bool
within x (a,b) = let
    lo = if a < b then a else b
    hi = if a < b then b else a
    in  x >= lo && x <= hi

isVertical :: LineSegment -> Bool
isVertical ((x1,y1),(x2,y2)) = x1 == x2

magnitude :: Point -> Int
magnitude (x,y) = abs x +  abs y

intersection :: LineSegment -> LineSegment -> Maybe Point
intersection l1@((x1,y1),(x2,y2)) l2@((x3,y3),(x4,y4)) = case (isVertical l1, isVertical l2) of
    (False, False) -> Nothing
    (True, True) -> Nothing
    (True, False) -> if (y3 `within` (y1, y2)) && (x1 `within` (x3, x4)) then Just (x1, y3) else Nothing
    (False, True) -> if (y1 `within` (y3, y4)) && (x3 `within` (x1, x2)) then Just (x3, y1) else Nothing

lineSegmentOfDirection :: Point -> String -> LineSegment
lineSegmentOfDirection p@(x,y) (d:ss) = let
    length = read ss
    (deltax, deltay) = case d of 
        'U' -> (0, length)
        'D' -> (0, -length)
        'R' -> (length, 0)
        'L' -> (-length, 0)
    in (p, (x + deltax, y + deltay))

segmentsOfDirections :: Point -> [String] -> [LineSegment]
segmentsOfDirections _ [] = []
segmentsOfDirections origin (x:xs) = let
    segment = lineSegmentOfDirection origin x
    nextpoint = snd segment
    in segment : (segmentsOfDirections nextpoint xs)

lineIntersections :: [LineSegment] -> [LineSegment] -> [Point]
lineIntersections redline blueline = catMaybes [ intersection a b | a <- redline, b <- blueline ]

lowestOf :: (a -> Int) -> [a] -> Int
lowestOf f xs = minimum [ f x | x <- xs ]

problem1 :: [String] -> [String] -> Int
problem1 redline blueline = let
    rs = segmentsOfDirections (0,0) redline
    bs = segmentsOfDirections (0,0) blueline
    in lowestOf magnitude (lineIntersections rs bs)

-- https://stackoverflow.com/a/4981265
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s = case dropWhile p s of
                    "" -> []
                    s' -> w : wordsWhen p s''
                        where (w, s'') = break p s'

getData :: IO ([String], [String])
getData = do
    handle <- openFile "inputs/03.txt" ReadMode
    contents <- hGetContents handle

    let lines = words contents
    let (redline, blueline) = (wordsWhen (\c->c==',') (head lines), wordsWhen (\c->c==',') (lines !! 1))

    return (redline, blueline)

main = do
    (red, blue) <- getData
    let s1 = problem1 red blue
    print "Solution 1: "
    print s1