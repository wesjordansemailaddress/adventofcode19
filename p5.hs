import System.IO  
import Control.Monad
import Data.Array.IO
import Data.Array.Base
import Text.Printf

type Opcode = Int
type Instruction = Int
type Address = Int
type Value = Int
type ParamMode = Int
type ParamModes = Int

type Memory = IOArray Int Int

digit :: Int -> Int -> Int
digit n x = (x `mod` (10^n)) `div` (10^(n-1))

opcodeOfInstruction :: Instruction -> Opcode
opcodeOfInstruction inst = inst `mod` 100

readInstruction :: Instruction -> (Opcode, ParamModes)
readInstruction inst = let
    opcode = opcodeOfInstruction inst
    pmodes = inst `div` 100
    in (opcode, pmodes)

---

debug3 :: Memory -> Address -> IO ()
debug3 m ptr = do
    inst <- m `readArray` ptr
    p1 <- m `readArray` (ptr+1)
    p2 <- m `readArray` (ptr+2)
    p3 <- m `readArray` (ptr+3)
    printf "%0.5d %d %d %d\n" inst p1 p2 p3

debug2 :: Memory -> Address -> IO ()
debug2 m ptr = do
    inst <- m `readArray` ptr
    p1 <- m `readArray` (ptr+1)
    p2 <- m `readArray` (ptr+2)
    printf "%0.5d %d %d\n" inst p1 p2

debug1 :: Memory -> Address -> IO ()
debug1 m ptr = do
    inst <- m `readArray` ptr
    p1 <- m `readArray` (ptr+1)
    printf "%0.5d %d\n" inst p1

loadInstruction :: Memory -> Address -> IO (Opcode, ParamModes)
loadInstruction m ptr = do
    inst <- m `readArray` ptr
    return . readInstruction $ inst

loadValue :: Memory -> Address -> ParamMode -> IO Value
loadValue m a 0 = m `readArray` a
loadValue _ a 1 = return a

loadParamAddress :: Memory -> Address -> Int -> IO Address
loadParamAddress m ptr offset = m `readArray` (ptr+offset)

loadParamValue :: Memory -> ParamModes -> Address -> Int -> IO Value
loadParamValue m pmodes ptr offset = do
    let pmode = digit offset pmodes
    --print $ show (offset, pmode)
    a <- loadParamAddress m ptr offset
    loadValue m a pmode

step :: Address -> Memory -> IO Memory
step ptr m = do
    print $ "pos: " ++ show ptr
    (opcode, pmodes) <- loadInstruction m ptr
    let loadParamValue' = loadParamValue m pmodes ptr
    let loadParamAddress' = loadParamAddress m ptr

    case opcode of
        1 -> do -- add (3)
            debug3 m ptr
            left <- loadParamValue' 1
            right <- loadParamValue' 2
            dest <- loadParamAddress' 3

            let res = left + right
            printf "(%d) %d %d -> #%d %d\n" opcode left right dest res

            writeArray m dest res
            step (ptr+4) m
        2 -> do -- mult (3)
            debug3 m ptr
            left <- loadParamValue' 1
            right <- loadParamValue' 2
            dest <- loadParamAddress' 3

            let res = left * right
            printf "(%d) %d %d -> #%d %d\n" opcode left right dest res

            writeArray m dest res
            step (ptr+4) m
        3 -> do -- in (1)
            debug1 m ptr
            dest <- loadParamAddress' 1
            print "Input >"
            input <- readLn

            printf "(%d) -> #%d %d\n" opcode dest input

            writeArray m dest input
            step (ptr+2) m
        4 -> do -- out (1)
            debug1 m ptr
            value <- loadParamValue' 1
            printf "Output: %d\n" value

            printf "(%d) %d\n" opcode value

            step (ptr+2) m
        5 -> do -- jnz (2)            
            debug2 m ptr
            value <- loadParamValue' 1
            next <- if value /= 0 then loadParamValue' 2 else return (ptr+3)
            printf "(%d) %d -> #%d\n" opcode value next
            -- write??
            step next m
        6 -> do -- jz (2)
            debug2 m ptr
            value <- loadParamValue' 1
            next <- if value == 0 then loadParamValue' 2 else return (ptr+3)
            printf "(%d) %d -> #%d\n" opcode value next
            -- write??
            step next m
        7 -> do -- lt (3)
            left <- loadParamValue' 1
            right <- loadParamValue' 2
            dest <- loadParamAddress' 3

            let res = if left < right then 1 else 0
            printf "(%d) %d %d -> #%d %d\n" opcode left right dest res

            writeArray m dest res
            step (ptr+4) m
        8 -> do -- eq (3)
            left <- loadParamValue' 1
            right <- loadParamValue' 2
            dest <- loadParamAddress' 3

            let res = if left == right then 1 else 0
            printf "(%d) %d %d -> #%d %d\n" opcode left right dest res

            writeArray m dest res
            step (ptr+4) m
        99 -> do
            print "halted"
            return m
        otherwise -> do
            print $ "Unexpected opcode: " ++ (show opcode)
            return m

problem1 :: Memory -> IO Memory
problem1 = step 0

problem2Instance :: Value -> Value -> [Value] -> IO Value
problem2Instance x y ints = do
    arr <- createMemory ints
    writeArray arr 1 x
    writeArray arr 2 y
    arr' <- problem1 arr
    arr `readArray` 0

findFirstIO :: (a -> IO Int) -> Int -> [a] -> IO (Maybe a)
findFirstIO _ _ [] = return Nothing
findFirstIO pred needle (x:xs) = do
    res <- pred x
    case res == needle of
        True -> return (Just x)
        False -> findFirstIO pred needle xs

problem2 :: [Int] -> Int -> IO (Maybe (Int, Int))
problem2 state needle = do
    let range = [ (x, y) | x <- [0..100], y <- [0..100] ]
    findFirstIO (\(x,y) -> problem2Instance x y state) needle range 

---

intsOfWords :: [String] -> [Int]
intsOfWords = map read

-- https://stackoverflow.com/a/4981265
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s = case dropWhile p s of
                    "" -> []
                    s' -> w : wordsWhen p s''
                        where (w, s'') = break p s'

-- adapted from: https://hackage.haskell.org/package/array-0.5.0.0/docs/src/Data-Array-Base.html#newListArray
newListArrayDef :: (MArray a e m, Ix i) => (i,i) -> e -> [e] -> m (a i e)
newListArrayDef (l,u) def es = do
    marr <- newArray (l,u) def
    let n = safeRangeSize (l,u)
    let fillFromList i xs | i == n    = return ()
                          | otherwise = case xs of
            []   -> return ()
            y:ys -> unsafeWrite marr i y >> fillFromList (i+1) ys
    fillFromList 0 es
    return marr

createMemory :: [Int] -> IO Memory
createMemory xs = newListArrayDef (0,1000) 0 xs

test :: [Int] -> IO [Int]
test xs = do
    z <- newListArrayDef (0,100) 0 xs  :: IO (Memory)
    z' <- problem1 z
    getElems z'

main = do
    handle <- openFile "inputs/05.txt" ReadMode
    contents <- hGetContents handle
    let ints = intsOfWords (wordsWhen (\c->c==',') contents)

    initState <- createMemory ints

    sln1 <- problem1 initState
    print "Solution 1: "
    sln1_elems <- getElems sln1
    print sln1_elems

    sln2 <- problem1 initState
    print "Solution 2: "
    sln2_elems <- getElems sln2
    print sln2_elems
    
    hClose handle 