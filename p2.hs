import System.IO  
import Control.Monad
import Data.Array.IO
import Data.Array.Base

type Memory = IOArray Int Int

step :: Int -> Memory -> IO Memory
step ptr m = do
    --print $ "pos: " ++ show ptr
    opcode <- m `readArray` ptr

    case opcode of
        1 -> do
            source_l <- m `readArray` (ptr+1)
            source_r <- m `readArray` (ptr+2)
            dest <- m `readArray` (ptr+3)

            left <- m `readArray` source_l
            right <- m `readArray` source_r

            let res = left + right
            writeArray m dest res
            step (ptr+4) m
        2 -> do
            source_l <- m `readArray` (ptr+1)
            source_r <- m `readArray` (ptr+2)
            dest <- m `readArray` (ptr+3)

            left <- m `readArray` source_l
            right <- m `readArray` source_r

            let res = left * right
            writeArray m dest res
            step (ptr+4) m
        99 -> do
            return m

problem1 :: Memory -> IO Memory
problem1 = step 0

problem2Instance :: Int -> Int -> [Int] -> IO Int
problem2Instance x y ints = do
    arr <- createMemory ints
    writeArray arr 1 x
    writeArray arr 2 y
    arr' <- problem1 arr
    arr `readArray` 0

findFirstIO :: (a -> IO Int) -> Int -> [a] -> IO (Maybe a)
findFirstIO _ _ [] = return Nothing
findFirstIO pred needle (x:xs) = do
    res <- pred x
    case res == needle of
        True -> return (Just x)
        False -> findFirstIO pred needle xs

problem2 :: [Int] -> Int -> IO (Maybe (Int, Int))
problem2 state needle = do
    let range = [ (x, y) | x <- [0..100], y <- [0..100] ]
    findFirstIO (\(x,y) -> problem2Instance x y state) needle range 

---

intsOfWords :: [String] -> [Int]
intsOfWords = map read

-- https://stackoverflow.com/a/4981265
wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s = case dropWhile p s of
                    "" -> []
                    s' -> w : wordsWhen p s''
                        where (w, s'') = break p s'

-- adapted from: https://hackage.haskell.org/package/array-0.5.0.0/docs/src/Data-Array-Base.html#newListArray
newListArrayDef :: (MArray a e m, Ix i) => (i,i) -> e -> [e] -> m (a i e)
newListArrayDef (l,u) def es = do
    marr <- newArray (l,u) def
    let n = safeRangeSize (l,u)
    let fillFromList i xs | i == n    = return ()
                          | otherwise = case xs of
            []   -> return ()
            y:ys -> unsafeWrite marr i y >> fillFromList (i+1) ys
    fillFromList 0 es
    return marr

createMemory :: [Int] -> IO Memory
createMemory xs = newListArrayDef (0,500) 0 xs

test :: [Int] -> IO [Int]
test xs = do
    z <- newListArrayDef (0,100) 0 xs  :: IO (Memory)
    z' <- problem1 z
    getElems z'

main = do
    handle <- openFile "inputs/02.txt" ReadMode
    contents <- hGetContents handle
    let ints = intsOfWords (wordsWhen (\c->c==',') contents)
    print "Input: "
    print ints

    initState <- createMemory ints

    sln1 <- problem1 initState
    print "Solution 1: "
    sln1_elems <- getElems sln1
    print sln1_elems

    sln2 <- problem2 ints 19690720
    print "Solution 2: "
    print sln2
    
    hClose handle 