import System.IO  
import Control.Monad
import Data.List
import Text.Printf

type PlanetName = String
type Orbit = (PlanetName, PlanetName)

data Planet = Planet PlanetName [Planet] deriving (Show)

firstMaybe :: (a -> Maybe b) -> [a] -> Maybe b
firstMaybe _ [] = Nothing
firstMaybe f (x:xs) = case f x of 
    Just n -> Just n
    Nothing -> firstMaybe f xs

satellites :: Planet -> [Planet]
satellites (Planet _ c) = c

readOrbit :: String -> Orbit
readOrbit s = let
    (front, rest) = break (\c->c==')') s
    in (front, tail rest)

readPlanets :: [Orbit] -> PlanetName -> Planet
readPlanets list name = let
    children = [s | (c,s) <- list, c == name]
    in Planet name (map ( readPlanets list ) children)

searchDepth :: PlanetName -> Planet -> Maybe Int
searchDepth pn (Planet name ss) = if pn == name then Just (-1) else case ss of
    []        -> Nothing
    otherwise -> do
        depth <- firstMaybe (searchDepth pn) ss
        return $ depth + 1

traversal :: PlanetName -> PlanetName -> Planet -> Maybe Int
traversal from to planet@(Planet _ ss) = case (firstMaybe (traversal from to) ss ) of 
    Just n -> Just n
    otherwise -> do
        d1 <- searchDepth from planet
        d2 <- searchDepth to planet
        return $ d1 + d2

---

problem1 :: Int -> Planet -> Int
problem1 d (Planet _ satellites) = d + sum (map (problem1 (d+1)) satellites)

---

getData :: IO Planet
getData = do
    handle <- openFile "inputs/06.txt" ReadMode
    contents <- hGetContents handle
    let l = map readOrbit ( sort ( lines contents ) )
    return $ readPlanets l "COM"

main = do
    com <- getData
    
    let res1 = problem1 0 com
    printf "Solution 1: %d\n" res1

    let res2 = traversal "YOU" "SAN" com
    printf "Solution 2: %s\n" (show res2)