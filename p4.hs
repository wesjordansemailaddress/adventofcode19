digit :: Int -> Int -> Int
digit n x = (x `mod` (10^n)) `div` (10^(n-1))

digits :: Int -> [Int]
digits x = let
    helper x xs = if x >= 0 && x <= 9 then x:xs else let
        (fore, aft) = (x `div` 10, x `mod` 10)
        in helper fore (aft:xs)
    in helper x []

increasing :: [Int] -> Bool
increasing [] = True
increasing (_:[]) = True
increasing (x:xs) = (x <= head xs) && increasing xs

repeating :: [Int] -> Bool
repeating [] = False
repeating (_:[]) = False
repeating (x:xs) = (x == (head xs)) || repeating xs

repeating' :: Int -> [Int] -> Bool
repeating' _ [] = False
repeating' _ (_:[]) = False
repeating' p (x:y:[]) = p /= x && x == y
repeating' p (x:y:xs) = (p /= x && x == y && y /= head xs) || repeating' x (y:xs)

increasingAndRepeating :: Int -> Bool
increasingAndRepeating x = let
    d = digits x
    in (increasing d) && (repeating d)

increasingAndRepeating' :: Int -> Bool
increasingAndRepeating' x = let
    d = digits x
    in (increasing d) && (repeating' (-1) d)

countWhere :: (a -> Bool) -> [a] -> Int
countWhere _ [] = 0
countWhere f (x:xs) = let
    p = if f x then 1 else 0
    in p + countWhere f xs 

problem1 :: Int -> Int -> Int
problem1 from to = countWhere increasingAndRepeating [from..to]

problem2 :: Int -> Int -> Int
problem2 from to = countWhere increasingAndRepeating' [from..to]

main = do
    let (from, to) = (254032, 789860)
    let s1 = problem1 from to
    print "Solution 1: "
    print s1

    let s2 = problem2 from to
    print "Solution 2: "
    print s2