import System.IO  
import Control.Monad

fuelForModule :: Integer -> Integer
fuelForModule moduleWeight = (moduleWeight `div` 3) - 2

fuelForModuleAndFuel :: Integer -> Integer
fuelForModuleAndFuel moduleWeight = let
    fuel = fuelForModule moduleWeight
    in case fuel <= 0 of
        True -> 0
        False -> fuel + fuelForModuleAndFuel fuel

problem1 :: [Integer] -> Integer
problem1 input = sum (map fuelForModule input)
        
problem2 :: [Integer] -> Integer
problem2 input = sum (map fuelForModuleAndFuel input)

intsOfWords :: [String] -> [Integer]
intsOfWords = map read

main = do
    handle <- openFile "inputs/01.txt" ReadMode
    contents <- hGetContents handle
    let ints = intsOfWords (words contents)
    print "Input: "
    print ints

    let sln1 = problem1 ints
    print "Solution 1: "
    print sln1

    let sln2 = problem2 ints
    print "Solution 2: "
    print sln2
    
    hClose handle   